package com.griffinkenya.matatu.utilities

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.widget.EditText
import com.griffinkenya.matatu.R

class CustomEditText@JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : EditText(context, attrs, defStyleAttr) {

    var error: Boolean = false
        set(value) {
            showError(value)
        }

    private fun showError(show: Boolean) {
        val background = if (show) R.drawable.background_edit_text_error else R.drawable.background_edit_text
        this.setBackgroundResource(background)
        this.addTextChangedListener(getTextWatcher())
    }

    private fun getTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                error = false
            }

            override fun afterTextChanged(s: Editable?) {
            }
        }
    }
}