package com.griffinkenya.matatu.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.griffinkenya.matatu.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
